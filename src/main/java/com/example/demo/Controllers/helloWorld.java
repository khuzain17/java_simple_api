package com.example.demo.Controllers;

import com.example.demo.Entity.UserName;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class helloWorld {

    @GetMapping("/")
    public String Hello () {
        return "Hello World";
    }

    @GetMapping("/Hello/{nama}")
    public String Hello(@PathVariable(value = "nama") String Nama) {  // yg di "value" harus sama dgn yg di path
        return "Hello " + Nama; // nama yg disini beda dgn yg di path
    }

    @PostMapping("/")
    public String HelloName(@RequestParam(value = "name") String name) {
        return "Hello " + name;
    }

    @PostMapping("/HelloUser")
    public UserName HelloWorldUserController(@Valid @RequestBody UserName UserName){ // public "UserName" disini merupakan panggilan dari Class UserName di Entity
        return UserName;
    }



}

