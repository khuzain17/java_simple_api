package com.example.demo.Entity;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class UserName {
    @Size(min = 3, max=50, message = "name between 3 - 50")
    private String Name;

    @Size(min = 3, max=50, message = "address between 3 - 50")
    private String Address;

    @Email(message="email is invalid")
    private String email;



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        //traditionally we put if else here
        this.email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
